// associo evento DOMContentLoaded alla funzione main
// (nodi HMTL tutti caricati)
document.addEventListener("DOMContentLoaded", function(){
    main();
});

async function addTodo(text){

    var newTodo = {description: text};

    await fetch('http://localhost:8080/api/ToDoList', {
         method: 'POST',
         headers:{'Content-Type': 'application/json'},// dobbiamo dire che è json
         body: JSON.stringify(newTodo)
    });
}


//js parte da questa funzione di base 
async function main(){
    var divApp= document.getElementById("app");
    var title = document.createElement("h1");
    title.textContent= "TO DO";
    divApp.appendChild(title);

    var todos= await fetch('http://localhost:8080/api/ToDoList');
    var response = await todos.json();

    console.log(response);

    var textinput = document.createElement('input');
    textinput.placeholder= "inserisci un nuovo todo";
    textinput.id = "texto";
    divApp.appendChild(textinput);

    var addButton = document.createElement('button');
    addButton.textContent = 'aggiungimi';
    addButton.addEventListener('click', function(){
        
        var todoDesc = document.getElementById('texto');
        addTodo(todoDesc);

    })
    divApp.appendChild(addButton);


    var ulist = document.createElement('ul');
    
    divApp.appendChild(ulist)

    for(var i=0; i< response.length; i++){
        var liElement = document.createElement('li');

        var todo = response[i];
        liElement.textContent = todo.description;
        if(todo.done){
            liElement.textContent+=' - Completato';
        }
        else{
            liElement.textContent+=' - Da completare';
        }
        ulist.appendChild(liElement);
    }

 }