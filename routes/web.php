<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/api/info', function () use ($router) {
    return "v 0.0.1";
});


$router->get('/api/ToDoList', function () use ($router) {
    //carico l'array di todos
    $results = app('db') -> select("SELECT * From tasks");
    return $results;
});


$router->get('/welcome', function () use ($router) {
    return "Welcome in this webpage";
});

$router->post('/api/ToDoList', "TasksController@add");
$router->put('/api/ToDoList/{id}', "TasksController@update");
$router->delete('/api/ToDoList/{id}', "TasksController@delete");