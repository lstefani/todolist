<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function add(Request $request){
      
        $description = $request->input('descrizione');

        $results = app('db')-> insert(
            "insert into tasks(description, done, insert_date) 
            values('$description', false, now())"
        );

        return new Response(null, 201); 
    }

    public function update(Request $request, $id){
      
        $description = $request->input('descrizione');
        $done = $request->input('fatto');

        $results = app('db')-> update(
            "UPDATE tasks SET 
            description='$description', done=$done WHERE id=$id"
        );

        return new Response(null, 200); 
    }

    public function delete(Request $request, $id){
      
        $results = app('db')-> delete(
            "DELETE from tasks WHERE id=$id"
        );
        return new Response(null, 204); 
    }



    
}
